---
layout: document
title: Welcome
no_title: true
no_header: true
excerpt: Hello, my name is Blaine and this is my homepage. I like to play with
    computers, fix broken things, and write software. This site is where I
    publish projects I’m working on and other odds and ends that I end up
    documenting.
---
{% include accent.html layout="
    neon-globe.gif
    | welcome.gif, open-24hr.gif
    | lucky-cat.gif" %}

Hello, my name is Blaine and this is my homepage. I like to play with
computers, fix broken things, and write software. This site is where I publish
[projects]({% link projects/index.md %}) I'm working on and other
[odds and ends]({% link scraps/index.md %}) that I end up documenting.

{% include accent.html layout="
    robot.gif
    | open-all-nite.gif
    | ufo.gif, welcome-net.gif
    | alien.gif
    | speaker.gif" %}

Occasionally I post [updates]({% link posts/index.md %}) about this site or
[other sites]({% link sites/index.md %}) I'm working on. This place is
perpetually under construction so be sure to check back often to see new
content. Thank you for stopping by :)

{% include accent.html layout="
    head-bash.gif
    | construction-seg.gif, construction-flip.gif
    | run-scissors.gif" %}
