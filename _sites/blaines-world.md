---
title: Blaine's World
external_url: https://blaines.world
order: 1
icon: icon.gif
---
Have a look around; you're soaking in it. This is where I post my personal
projects and other tinkering-related scraps.
