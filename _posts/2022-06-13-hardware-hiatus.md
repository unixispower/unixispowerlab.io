---
layout: document
title: Hardware Hiatus
date: 2022-06-21 22:16:25 +0000
---
I moved in with family earlier this year. Almost all of my tools and parts are
now in a storage unit with the exception of a portable "kit" I compiled as I was
packing for the move. I have a basic soldering iron, hot air rework tool,
multimeter, portable power supply, and a fishing lure organizer full of wire
and passive components.

What I do _not_ have is easy access to my collection of half-baked projects and
scrap materials. I also do not have much room to work on large physical projects
like I did before the move. Essentially, my hardware projects have been put on
hold until I am fully relocated (hopefully later this year).

Although I am separated from my hardware, I do not feel the urge to make things
waning. Instead, I have been working on things of a more digital nature like
[art](https://mystichybrid.info) and code. I'm currently designing a couple of
websites that I plan to release within the next few months. As soon as things
are public they will be added to the shiny new
[sites section]({% link sites/index.md %}).
