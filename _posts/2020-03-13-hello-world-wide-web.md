---
layout: document
title: Hello World Wide Web
date: 2020-03-13 07:59:59 +0000
---
This is my first "official" personal blog post. I have always wanted a place to
scratch down my thoughts, but I have avoided creating a blog for some time.
It's not an uncommon occurrence to wander through the internet and stumble upon
an abandoned tech blog or a boilerplate WordPress setup that has a single test
post. In the past I figured any personal attempt to start a weblog would have
the same fate, but today I finally found the initiative to start one anyway.
