---
layout: project
title: Chanterelle
date: 2017-05-21 02:15:26 +0000
icon: icon.gif
links:
  - title: Source
    description: Program source code
    url: https://gitlab.com/unixispower/chanterelle
  - title: PyPi Package
    description: Python package
    url: https://pypi.org/project/chanterelle/
---
A command line utility for uploading static site files to an S3 bucket. I
created this simple utility after encountering issues with MIME type detection
and costly ($) API calls in the official AWS tools.
