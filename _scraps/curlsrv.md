---
layout: project
title: cURLsrv
date: 2022-07-18 22:30:17 +0000
icon: icon.gif
links:
  - title: Source
    description: Web server source code
    url: https://gitlab.com/unixispower/curlsrv
---
Web server that hosts cURL-able terminal animations. This is nothing more than
a small Flask app that hosts files at a fixed transfer rate. There's also some
user agent filtering to keep non-cURL clients out.

See it in action:

```shell
curl curl.blaines.world
```

Should produce the following on a VT100 compatible terminal:

{% include image.html filename="screenshot.gif" pixelated=true wide=true
    alt="Animation of cURL fetching a file served with cURLsrv" %}
