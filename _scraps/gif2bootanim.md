---
layout: project
title: gif2bootanim
date: 2020-04-07 23:27:00 +0000
icon: icon.gif
cover: phone.gif
links:
  - title: Conversion Script
    description: Shell script that converts from GIF to boot animation
    url: https://gitlab.com/unixispower/gif2bootanim
---
A simple shell script that converts animated GIF files into `bootanimation.zip`
files that are compatible with Android. I wrote this to convert GIFs from
[GifCities](https://gifcities.org/) into boot animations for my phone.

The script takes an animated gif like the following:

{% include image.html filename="input.gif" pixelated=true
    alt="Animated GIF input image" %}

And converts it into a boot animation for an Android device:

{% include image.html filename="phone.gif"
    alt="Phone showing the generated boot animation" %}
