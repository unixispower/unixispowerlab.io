---
layout: project
title: J-Card Template
date: 2017-08-27 19:25:12 +0000
icon: icon.gif
cover: jcard-front.jpg
links:
  - title: Online Template
    description: Live template ready to use in-browser
    url: https://unixispower.gitlab.io/jcard-template/
  - title: Source Code
    description: Jekyll source code used to generate template
    url: https://gitlab.com/unixispower/jcard-template
---
A printable in-browser template for creating cassette tape J-Cards. I created
the template for a personal project, but ended up releasing it after finding
several thriving online cassette communities.

{% include image.html filename="jcard-front.jpg"
    alt="J-card inside a cassette tape case" %}
