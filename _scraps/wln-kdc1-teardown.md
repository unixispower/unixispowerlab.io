---
layout: project
title: WLN KD-C1 Teardown
date: 2018-01-11 02:44:00 +0000
icon: icon.gif
cover: pcb-lifted.jpg
---
I tried to find a teardown of the KD-C1 before I purchased it. I managed to 
find a [video](https://www.youtube.com/watch?v=zIZWfzFL2QY) of a partial
teardown, but this video stops short of removing the PCB from the  plastic case
(for good reason as seen below). I decided to go deep and completely
disassemble the radio as far as I could.

{% include image.html filename="wln-kdc1.jpg" alt="WLN KD-C1 radio" %}

The belt clip and battery cover were easy to remove without tools. Below the 
battery cover there were 6 Torx screws holding the plastic halves of the radio 
together. One screw was hiding under the battery compartment label.  

{% include image.html filename="clip-battery-removed.jpg"
    alt="Radio with belt clip and battery removed" %}

Once the screws were removed the two plastic halves separated easily.

{% include image.html filename="back-removed.jpg"
    alt="Radio with back half removed exposing PCB" %}

This is the point where the teardown I found stopped. The radio charges via 
USB or in a charge cradle that connects to the PCB via metal contacts on the 
bottom of the plastic case. These metal contacts are soldered directly onto 
the PCB making removal from the case impossible without desoldering or cutting 
them.  

{% include image.html filename="contact-closeup.jpg"
    alt="Battery charging contacts soldered to PCB" %}

I decided to take the risk of melting the case and used a ColdHeat portable 
soldering iron and solder sucker to detach the tabs. After using the solder 
sucker I was able to push the tabs through the holes they were installed in. I 
melted the case just a bit, but it shouldn't be too obvious once the tabs are 
replaced.  

{% include image.html filename="contact-removed.jpg"
    alt="Radio with battery contact removed" %}

The antenna and speaker wires were also soldered to the PCB and needed to be 
desoldered before the board was removed. I wasn't able to completely remove 
the antenna from the plastic case because it seems to be molded or glued into 
the plastic. To work around this I tucked the antenna under the board once I 
desoldered it.  

{% include image.html filename="antenna-tucked.jpg"
    alt="Antenna desoldered and tucked under PCB" %}

After the tabs, speaker, and antenna were disconnected the only remaining 
restraint was a nut hidden below the volume/power knob. I removed the knob by 
pulling it off the potentiometer. I was able to loosen the nut by wedging a 
flat head screwdriver in one of the grooves and pushing to cause the nut to 
spin counter-clockwise  

{% include image.html filename="pot-nut.jpg"
    alt="Nut attaching potentiometer to case" %}

Once all impeding connections were been broken I (finally) removed the PCB 
from the case. I used a screwdriver to lift the right side of the board and 
pulled it out by the lower right corner. This step was a little tricky and 
took some patience.  

{% include image.html filename="pcb-lifted.jpg"
    alt="PCB pried away from casing using a screwdriver" %}

I took some photos of the board once I got it out. I tried to get clear shots 
of all the ICs and traces in case someone finds them useful.  

{% include image.html filename="pcb-back.jpg" alt="Back view of PCB" %}

Here is a shot of the side of "font" of the PCB that is hidden by the case.

{% include image.html filename="pcb-front.jpg" alt="Front view of PCB" %}

There are several 8-pin packages on the front of the board. One IC is labelled 
with the model number of the radio "KD-C1", the other is labelled "HK24C08".  

{% include image.html filename="pcb-front-bottom.jpg"
    alt="Bottom of the front of the PCB" %}

This one is labelled "M4871".

{% include image.html filename="pcb-front-top.jpg"
    alt="Top of the front of the PCB" %}

There is also what looks to be a custom microcontroller "TA3767P" and a radio 
transceiver IC "AT1846S" on the back of the board. I had to remove a metal 
shield to get access to the transceiver IC.

{% include image.html filename="pcb-back-bottom.jpg"
    alt="Bottom of the back of the PCB" %}

I didn't come up with much searching for the AT1846S. I found a
[general description](https://img.ozdisan.com/ETicaret_Dosya/500549_6108302.pdf)
posted by a supplier, but I couldn't find a datasheet. The general description 
states the IC is portable FM radio transceiver that works in the ranges 
134MHz-174MHz, 200MHz-260MHz, and 400MHz-520MHz. At first glance this IC looks 
similar to the 
[RDA1846](https://github.com/phishman/RDA1846/tree/master/Datasheets) (aside 
from the "S" suffix) that is in the Baofeng UV-3R radios. The layout of the 
AT1846S on the PCB is similar to the pinout of the RDA1846 from its datasheet 
meaning the two might be pin-compatible.

This has been a promising teardown. I am pleased to find out that the radio's 
transceiver IC might officially support 1.25m frequencies. Now I just need 
access to a spectrum analyzer and some radio testing equipment so I can get my 
hand dirty with modifications.
