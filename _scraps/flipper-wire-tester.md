---
layout: project
title: Flipper Wire Tester
date: 2023-08-13 04:06:55 +0000
icon: icon.gif
links:
  - title: Application Source
    description: C source code of Flipper Zero application
    url: https://gitlab.com/unixispower/flipper-wire-tester
---
A simple Flipper Zero app that beeps if a wire is continuous. Not nearly the
utility of a full multimeter, but sounds just like one.
