---
layout: project
title: Flipper Price Tagger
date: 2023-08-13 05:51:54 +0000
log_date: 2023-08-13 05:51:54 +0000
icon: icon.gif
flair: in-progress
links:
  - title: Application Source
    description: C source code of Flipper Zero application
    url: https://gitlab.com/unixispower/flipper-price-tagger
---
A Flipper Zero app that programs electronic shelf labels (ESL). Made for a
bounty request (but really because I've always wanted a way to use eink tags
as static displays for art).

This is currently in early stages of development. Watch this space for updates.
