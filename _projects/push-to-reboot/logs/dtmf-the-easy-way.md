---
layout: document
title: DTMF the Easy Way
date: 2018-04-18 18:55:00 +0000
categories: [push-to-reboot, logs]
cover: module-mod.jpg
---
When I was brainstorming for this project I debated using a dedicated DTMF 
decoder IC or decoding tones using software and digital signal processing. The 
microcontroller I'll be using for this project is an Arduino Nano clone which 
is based on an ATmega328P microcontroller. This microcontroller has a couple 
of libraries available for DTMF decoding, but I can't find any such libraries 
that include tests or evidence that they work reliably in the real world. I 
decided to take the easy way out and buy a few DTMF decoder modules that use 
the
[MT8870D decoder IC](https://www.microsemi.com/document-portal/doc_view/127041-mt8870d-datasheet-oct2006).

I ordered the 
[modules](https://www.amazon.com/HiLetgo-Decoder-Decoding-Telephone-Arduino/dp/B01DKAHXNI/) 
from Amazon, but the same blue module seems to be available from a variety of 
online sellers. For some reason all the modules I've seen have the pins 
installed "upside down". I'm not sure what the logic behind this was, but it 
makes prototyping a bit more difficult. I corrected this issue by removing the 
pins and installing them on the bottom of the boards so I can easily use the 
modules on breadboard and perfboard. To the left is a module as it is 
assembled, to the right is a module that I modified.

{% include image.html filename="module-mod.jpg"
    alt="DTMF module modification" %}

I was curious about what all the extra interface circuitry was on the module, 
so I looked up
[schematic for the module](https://www.openimpulse.com/blog/wp-content/uploads/wpsc/downloadables/MT8870-Schematic-Diagram.pdf);
and hit up the
[chip's datasheet](https://www.microsemi.com/document-portal/doc_view/127041-mt8870d-datasheet-oct2006). 
I found the schematic for the module by Googling and ended up at this
[product page](https://www.openimpulse.com/blog/products-page/product-category/mt8870-dtmf-audio-decoder-module/). 
It turns out the MT8870D can be configured for a wide variety of use cases 
that can affect the timing of DTMF digits. The module utilizes an example 
circuit exactly as it appears in the datasheet.

{% include image.html filename="module-schematic.png"
    alt="DTMF module schematic" %}

The table at the end of the datasheet indicates that the example configuration 
should accept 40ms mark and space durations and reject any durations that are 
less than 20ms. I plugged the module into a breadboard and verified this was 
the case using and Android app called
[ToneDef](https://play.google.com/store/apps/details?id=com.bytestemplar.tonedef&amp;hl=en_US). 
After I wired up the module I created a
[firmware repository](https://gitlab.com/unixispower/push-to-reboot) on GitLab
and  hacked together a simple driver to echo received digits over the serial
line.

I have only done basic testing, but the MT8870 module seems to be working very 
well. I may try to release a version of the firmware in the future where DTMF 
decoding is done in software, but for now the low cost and high accuracy of 
the decoder modules is very attractive.
