---
layout: document
title: Relays Under Control
date: 2018-04-20 03:09:00 +0000
categories: [push-to-reboot, logs]
cover: breadboard-relays.jpg
---
Today I added some core features to the PTR firmware. I made a more complete 
driver for the MT8870 DTMF decoder module that only accepts messages that 
start with `*` and end with `#`. I added a couple of other 
features like a character timeout and bad message rejection to simplify other 
parts of the firmware that handle the DTMF input. I also started a basic 
driver for the
[SainSmart 4-channel 5V relay module](https://www.sainsmart.com/products/4-channel-5v-relay-module);
that  I added to control small AC loads. This driver is dead simple; it
abstracts the inverted logic of the module to make higher level code simpler.

{% include image.html filename="breadboard-relays.jpg"
    alt="Relays connected to breadboard circuit" %}

The biggest feature I added was command processing. The current firmware is 
capable of fully controlling the relay module via the DTMF command protocol 
described in an
[earlier log](https://hackaday.io/project/134298-push-to-reboot/log/144099-protocol-ideas). 
Command processing is fully functional, but the firmware still lacks many base 
features. Right now settings like node number, reboot duration, and call 
duration are hard coded. In the future these settings and others will be set 
by a computer via USB serial interface. The current firmware also lacks any 
sort of persistence of state to EEPROM. Port state needs to be stored and 
restored at startup so correct operation can be restored in the case of a 
power fault.

I'll be working on the mentioned features and more this coming week. I'm also 
planning to experiment with real radios instead of simulating a radio link 
with my phone. Using real radios means I can also start work on the interface 
to send audio from node to controller. There is much to be done.
