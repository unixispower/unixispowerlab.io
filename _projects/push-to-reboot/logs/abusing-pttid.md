---
layout: document
title: Abusing PTT-ID
date: 2018-04-13 21:37:00 +0000
categories: [push-to-reboot, logs]
---
I recently purchased a [SharkRF 
openSPOT](https://www.sharkrf.com/products/openspot/) to feed my hardware 
addiction. Initially I installed it next to my computer and the rest of the IP 
networking equipment, but after a few days of playing I wanted to see if I 
could access it from a nearby park. I decided to increase elevation and add an 
external antenna, but to do so I had to move the setup to my attic. I managed 
to stuff the openSPOT, a wireless router, and some power supplies into a cheap 
plastic ammo can that lives happily upstairs for now.

Through testing and fiddling with the network I managed to climb up the 
rickety ladder to the attic at least a half dozen times. Each time I ascended 
I wondered how many more trips I would get away with before the folding ladder 
decided it wanted to become a set of individual ladders and leave me at the 
bottom. I also wondered how long it would be before I walked all the way to 
the park only to realize the network equipment was unresponsive.

After thinking for a while I came up with an idea to remotely control outlets 
by (ab)using a feature called [PTT-ID 
(ANI)](https://en.wikipedia.org/wiki/PTT_ID) that is built into many handheld 
radios. PTT-ID enables the radio to self identify before and/or after each 
transmission by sending out a 5 digit code as DTMF tones. This code is 
intended to be decoded by other radios to identify the transmitting party, but 
it can be used to send any arbitrary string of DTMF codes to other radios.

The idea I came up with consists of a "controller" radio that is programmed to 
broadcast DTMF commands and one or several "nodes" that receive and interpret 
the sent commands. Each node consists of a UHF handheld radio attached to a 
microcontroller that sets the state of relay-controlled outlets when a command 
is received. Each outlet is referred to as a "port" on the node. The 
controller and all nodes operate on a single frequency (simplex) and use a 
simple addressing scheme to determine which node acts on a broadcasted 
command.

The format of the commands sent by the controller is simple:

```
<node><port><action>#
```

Where "node" and "port" are a single digit numbers and "action" is a digit in 
"A, B, C, D". Asterisk and pound are used as delimiters to reduce the chance 
of a non-controller DTMF transmission being interpreted as a command. For 
example, if you wanted the first outlet on the first node to perform the "A" 
action you would send the command `*00A#`. After performing the action, 
the node would respond with a confirmation tone to indicate a command was 
received and an action was performed.

Right now this is all just an idea. I have ordered a DTMF decoder and relay 
board from Amazon and now I'm playing the waiting game. Once the parts 
arrive I can prototype a couple of nodes and start experimenting with the 
cheap radios I have laying around.
