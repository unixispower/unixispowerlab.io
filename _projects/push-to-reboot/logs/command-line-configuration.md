---
layout: document
title: Command Line Configuration
date: 2018-04-29 01:26:00 +0000
categories: [push-to-reboot, logs]
cover: cli.png
---
I finished a couple more features to the PTR firmware this week. The primary 
feature added was a command line interface for managing device settings over 
USB serial. Currently this interface supports setting and showing individual 
values, showing all values, resetting all values to default, and showing help 
and version screens. Configurable values are node number, reboot duration, and 
call duration. The following is a screenshot of the output of the `help`
command in [Minicom](https://en.wikipedia.org/wiki/Minicom):

{% include image.html filename="cli.png" alt="Command line interface screenshot" %}

Initially this was useless because a power cycle knocked out all settings as 
they were stored in RAM. To fix this I added code to save the configuration to 
the ATmega's EEPROM memory whenever a value is changed. The current firmware 
also stores the state of the node's ports after a command is completed. This 
allows the device to restore to the previous state in the case of a power 
failure or equipment migration.

Along the way I learned more about the architecture of the ATmega. After I 
added the initial command line code, the firmware's data segment bloated to 
over 1kB. I did some research and found this was expected behavior because 
the AVR utilizes a
[modified Harvard architecture](https://en.wikipedia.org/wiki/Modified_Harvard_architecture). 
Constant and literal values are copied to RAM on startup which causes a ton of 
memory to be wasted if these values are large. I was able to overcome this 
limitation by using the macros in AVR Libc
[pgmspace.h](https://www.nongnu.org/avr-libc/user-manual/pgmspace.html) 
to store string literals in program space. AVR Libc provides several string
functions that operate on strings stored in program space natively, so 
converting the existing code was very straightforward.

There is still much to be added before an initial release can be made. My next 
task is going to be to create a transmit interface for the PTR controller so 
confirmation tones can be sent back to the controller radio.
