---
layout: document
title: Locking Things Down
date: 2018-05-24 09:04:00 +0000
categories: [push-to-reboot, logs]
cover: secret-command.png
---
Over the past week I added security features to the PTR firmware. Until now it 
was possible for anyone with a DTMF capable radio to issue commands to any PTR 
node within range. I wanted to prevent unauthorized control of my devices 
(however unlikely that may be) so I added one-time-password authentication to 
the protocol.

Amateur radio laws in the U.S.
[prohibit the obscuring of messages](https://www.law.cornell.edu/cfr/text/47/97.113),
but permit the use of access security mechanisms as long as the meaning of the
underlying message being transmitted remains intelligible. To meet this
requirement I implemented HOTP based authentication compatible with
[RFC 4226](https://tools.ietf.org/html/rfc4226). HOTP utilizes a shared secret
key and a counter to generate a sequence of one-time-use passwords that can be
sent "in the clear" alongside the command messages of the push to reboot 
protocol.

The use of HOTP authentication in the PTR protocol is very simple. A 
controller radio issues a 6-digit one-time-password (OTP) to all nodes on the 
same frequency. When a node receives an OTP it checks it against a set of 
locally generated OTPs for a match. If a valid OTP is entered the node 
"unlocks" and interprets subsequent command messages that are sent within a 
fixed time window. This authentication provides minimal security without 
sacrificing the ability to use the protocol from a radio keypad.

HOTP requires a secret be shared between client and the authenticating party. 
This secret can be entered into the serial interface of a node as a base 32 or 
hexadecimal string. The following is a screenshot of a dummy code being 
entered into the serial interface:

{% include image.html filename="secret-command.png"
    alt="Screenshot of auth secret command in CLI" %}

I ended up implementing all of the HOTP algorithms (SHA1, HMAC, base 32, and 
hex) from scratch to meet the memory requirements of the ATmega328. This added 
some development time, but the trade-off was more C experience and a codebase 
that has no external dependencies aside from avr-libc. I chose HOTP over 
[TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm) 
because the ATmega doesn't have a real time clock and I didn't want to add 
anymore hardware to the project than I had to. I also took the opportunity to 
write tests for all of the algorithms and other bits of portable code in the 
project to ensure my implementations were correct.

Because the authentication is RFC 4226 compatible, common applications like 
[Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&amp;hl=en_US) 
can be used to generate one-time passwords. I successfully used Authenticator 
to generate codes and send commands to my prototype. I plan to provide a video 
demonstration of this once I finish the PTR hardware prototype.

Security was the biggest hurdle in this project to date. Now that the hard 
code is written I can focus on finishing the simple hardware features that 
remain: a TX radio interface and local controls to override each outlet on a 
node.
