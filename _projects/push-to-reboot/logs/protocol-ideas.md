---
layout: document
title: Protocol Ideas
date: 2018-04-15 18:02:00 +0000
categories: [push-to-reboot, logs]
---
In the last log I started to define a simple protocol that fits in a 5 
character DTMF PTT-ID. I have since thought of some additions that would make 
the protocol more useful and secure. I'm using this log to capture some of 
these ideas and open them for discussion/suggestion.

Each node has an ID number assigned to it. This ID was defined as zero-indexed 
in the last log, but I think it would be more intuitive to reserve ID 0 for a 
special case and start node and port numbering at 1. One-indexed addressing 
would mean node 1 and port 1 would have address 1 instead of 0 which (to me) 
feels more natural when using a numerical keypad. The use of 0 would then be 
to [multicast](https://en.wikipedia.org/wiki/Multicast) a command to 
all nodes or an action to all ports. For example, the command `` *00A# `` 
would now perform action A on all ports of all nodes. To address the first port
of the first node (as in the last log) the command would be `` *11A# ``.

I've mentioned "actions" for ports, but until now I haven't defined any. 
Actions are assigned to the DTMF letter digits A, B, C, and D. The commands 
containing these actions will most likely be programmed as PTT-IDs, but there's
a chance at some point they will be keyed in manually. I have assigned verbs 
to each action to make programming and manual keying easier.

| Action | Verb       | Description                                 |
|--------|------------|---------------------------------------------|
| A      | Activate   |Turn the port(s) on.                         |
| B      | (re)Boot   |Turn the port(s) off for 5 seconds, then on. |
| C      | Call       |Turn the port(s) on for 5 seconds, then off. |
| D      | Deactivate |Turn the port(s) off.                        |

After expanding the capabilities of the controller to deactivate all devices 
on multiple nodes I started to worry a bit about security. The ability to 
multicast is very useful and very dangerous. Without safeguards in place any 
radio owner is able to control the power of any device they can find. This is 
obviously no good, so some form of authentication must be used.

After looking around for existing telecommand authentication methods I found a 
very interesting video about [telecommand using authenticated APRS 
messages](https://www.youtube.com/watch?v=F-Da6deKW5k). The method described 
is a form of [time-based one-time password (TOTP) 
algorithm](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm). 
The general idea is to send a token from sender to receiver that verifies the 
identity the sender. A secret like a password is held secretly by both the 
sender and receiver. This secret is salted with the current timestamp and 
hashed to create a token which is then sent from the sender to the receiver. 
When the receiver receives the token it generates a new token using the same 
process and compares the two. These two tokens will only match if the secret 
and timestamp matches on both ends. The token is time-sensitive and 
secret-based meaning the secret must be known to create it and it cannot be 
reused after a short period of time by a third party.

I really like the method described in the talk by K7UDR because it's [a 
standard](https://tools.ietf.org/html/rfc6238) that is currently in use. There 
is, however, one small problem with the proposed method. Both the sender and 
receiver must have accurate and synchronized time sources. This means the use 
of a real time clock and/or the use of network time synchronization. I would 
like to avoid the extra complexity and additional hardware that would be 
necessary when using time-based token generation. After searching a bit more I 
found another algorithm that generates something called an [HMAC-based 
one-time password 
(HOTP)](https://en.wikipedia.org/wiki/HMAC-based_One-time_Password_algorithm). 
Like a TOTP, an HOTP is generated using a changing salt to prevent replay 
attacks, but unlike a TOTP an HOTP doesn't require a time source to be 
generated. An HOTP is generated using a counter that is incremented by both 
sender and receiver after every successful authentication. I think this is a 
good compromise for the application because it allows some security without 
increasing the cost of each node being built.

I wanted to get these ideas out and written down before working on any 
software or hardware. I think this protocol will be convenient and secure, but 
there are probably a lot of issues I haven't thought of. The DTMF boards have 
arrived from Amazon so I get started on a basic firmware while I think of more 
protocol ideas.
