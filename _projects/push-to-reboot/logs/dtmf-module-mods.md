---
layout: document
title: DTMF Module Mods
date: 2018-04-24 14:22:00 +0000
categories: [push-to-reboot, logs]
cover: module-mods.jpg
---
Yesterday I tested a cheap radio with the Push to Reboot prototype. Until now 
I have been testing the prototype using a DTMF generator app on my phone and a 
standard 3.5mm TRS auxiliary cable. I switched to a 3.5mm to 2.5mm cable and 
attached the output of an
[Amcrest ATR-22](https://amcrest.com/amcrest-atr-22-uhf-portable-radio-walkie-talkie-frequency-range-400-470-mhz-fm-transceiver-16-programmable-channels-high-power-flashlight-walkie-talkie-two-way-radio-fcc-certified.html) 
directly to the input of the DTMF module. I hit a couple snags along the way, 
but nothing came up that wasn't too easy to fix.

{% include image.html filename="breadboard-radio.jpg"
    alt="Test breadboard with radio attached" %}

The radio I attached to the node prototype is one I snagged on Amazon for 
about $15 (I bought a 
[2-pack](https://www.amazon.com/Amcrest-Baofeng-Upgraded-Channels-Flashlight/dp/B073WNM7NT) 
for 30$). I chose this radio because it was cheap and had FCC certs, but any 
radio with a
["Kenwood" headset pinout](https://ham.stackexchange.com/questions/1891/whats-the-pinout-for-kenwood-2-5mm-trs-3-5-mm-trs-connector) 
should work. I used the keypad of my TYT MD-380 to send DTMF tones to the 
ATR-22, but the DTMF module showed no input. I double checked the schematic of 
the radio's headset and DTMF module and found an issue. I got the schematic of
the headset connector from the
[The (Chinese) Radio Documentation Project](http://radiodoc.github.io/uv-5r/)
and the schematic of the module from a
[product page](https://www.openimpulse.com/blog/products-page/product-category/mt8870-dtmf-audio-decoder-module/)
I found.

{% include image.html filename="headset-schematic.png"
    alt="Kenwood headset compatibility schematic" %}

The radio uses the tip of the audio connector for audio while the module uses 
the ring. It is simple to modify the module to make the two compatible: the 
module needs to be rewired to receive audio in the tip of the TRS connector 
instead of the sleeve. The module I received uses a connector with exposed 
contacts. I used a pair of diagonal cutters to remove the middle contact then 
bridged the tip and sleeve pads on the underside of the board with solder.

{% include image.html filename="module-mods.jpg" alt="Module modifications" %}

After this modification the DTMF module started receiving audio. I entered a 
few test commands on the keypad of the MD-380 at close range and all worked 
perfectly. After testing with the keypad I loaded up several commands as 
PTT-IDs to be sent on key-up of the PTT button. To my surprise most commands 
sent this way were ignored by the node. I removed the cables from the ATR-22 
and listened to the tones sent by the MD-380 directly. For some reason the 
MD-380 does not always send DTMF tones correctly when they are sent as 
PTT-IDs. Almost every transmission was missing at least 1 DTMF digit and some 
transmissions didn't send DTMF tones at all. I tried repeating the test after 
creating a fresh codeplug and reverting to a stock firmware, but nothing fixed 
the issue. I can only assume this is a bug in the current MD-380 firmware 
(v13.20).

Aside from finding a bug in my radio this test was a success. I found a 
receiving setup that only required minimal modification of the DTMF module and 
allows me to use standard audio jumper cables. Now that a radio is attached I 
can get to work on a transmitting interface for sending confirmation tones.
