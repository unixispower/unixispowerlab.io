---
layout: document
title: Now In Stereo
date: 2017-05-25 05:14:00 +0000
categories: [wasteland-radio, logs]
cover: board-mounted.jpg
---
This past week I had a couple days off work, so I decided to make use of this 
time to mount new speakers in the radio cabinet. I picked up some 1/4" plywood 
to act as a adapter to fit the two new speakers into the hole left by the old 
one.

{% include image.html filename="speaker-board.jpg"
    alt="Plywood board for speakers" %}

I used the old speaker as a guide to mark where mounting holes needed to be 
drilled.

{% include image.html filename="speaker-measurement.jpg"
    alt="Old speaker traced on plywood board" %}

I then drilled mounting holes to match the oval ones on the old speaker. These 
greatly eased mounting the plywood in the cabinet because the existing posts 
were angled outward from the center of the speaker cutout.  

I'm not sure why, but the old speaker was mounted off-center to the vertical 
bars on the front of the radio. I compensated for this by drawing guides on 
the plywood that were square and centered with respect to the piece of wood 
the old speaker was mounted on -- not the hole left by the old speaker. This 
also compensated for the misalignment of the mounting holes on the plywood (I 
should have tried harder to align the old speaker before tracing it).

{% include image.html filename="board-fit.jpg"
    alt="Board test-fitted in cabinet" %}

The car speakers came with mounting brackets that made great guides for 
cutting holes in the plywood.

{% include image.html filename="speaker-brackets.jpg"
    alt="Speaker brackets on speaker board" %}

I used a band saw to cut holes to mount the new speakers. These holes were
slightly larger than the ones in the plastic brackets  

{% include image.html filename="speaker-holes.jpg"
    alt="Holes cut out for speakers" %}

Here are the speakers mounted in the plywood adapter. I used screws from a 
recent microwave oven tear-down to secure them in place.

{% include image.html filename="speakers-mounted-front.jpg"
    alt="Front of mounted speakers" %}

I picked up some rubber washers to install between the plywood and wood of the 
cabinet to prevent vibration noise.

{% include image.html filename="rubber-washers.jpg"
    alt="Rubber washers" %}

I re-used the original nuts with some new washers to secure the plywood on the 
posts in the cabinet.

{% include image.html filename="speakers-mounted-back.jpg"
    alt="Back of mounted speakers" %}

Admittedly this isn't the first hack at making an adapter for these speakers. 
Two previous incarnations had various flaws that prevented them from being 
used. I used the top (attempted) adapter as scrap wood to mount the AC power 
inlet in the cabinet.  

{% include image.html filename="bad-board-attempt.jpg"
    alt="Failed attempt at making a speaker board" %}

I cut a small piece off of this scrap to hold the AC inlet. I used a band saw
to cut the hole in the middle.

{% include image.html filename="ac-inlet-parts.jpg" alt="AC inlet parts" %}

I sourced two more screws from the microwave oven pile and used them to secure 
the inlet in place.

{% include image.html filename="ac-inlet-front.jpg" alt="AC inlet front view" %}

I soldered on wire leads to the inlet and covered the exposed bits with 
heat-shrink tubing.

{% include image.html filename="ac-inlet-back.jpg" alt="AC inlet back view" %}

Two screws hold the inlet assembly to the shelf in the cabinet that previously 
held the radio circuitry.

{% include image.html filename="ac-inlet-mounted.jpg"
    alt="AC inlet mounted in cabinet" %}

Here's what the cabinet looks like after the new parts were installed.

{% include image.html filename="board-mounted.jpg"
    alt="Speaker board mounted in cabinet" %}

A big thanks to my friend Kai who was nice enough to let me take up space in 
his workshop and use his collection of power tools.  
