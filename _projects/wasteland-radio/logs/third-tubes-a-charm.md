---
layout: document
title: Third Tube's a Charm
date: 2018-08-13 01:16:00 +0000
categories: [wasteland-radio, logs]
cover: coby-test.jpg
---
In the previous log I broke down the RCA TV that I chose to use for a display. 
I incorrectly assumed the TV was functional and proceeded to dismantle it 
without testing it. It turns out that when powered up it had some serious 
issues. At first the tube appeared lifeless while the high voltage transformer 
emitted high pitch clicks at random intervals. Each click was infrequent, but 
as time went on the rate increased so much that it almost sounded like white 
noise. After about 20 seconds the tube began to glow, but was interrupted and 
flickered during some loud pops. I was only brave enough to test the TV once 
and I turned it off when it started flickering because I feared it would be 
damaged if left on any longer. What little picture that was on the screen was 
out of focus and almost entirely white.

{% include image.html filename="rca-test.jpg" alt="RCA TV power-on test" %}

I decided to put the RCA aside and try out a different display that I had 
handy: a Coby CX-TV1. The Coby is a small combination TV/radio that operates 
on 12V DC. Like the RCA TV the Coby has a video input, but unlike it the 
display is black and white.

{% include image.html filename="coby-front.jpg" alt="Coby TV front view" %}

I found the Coby TV for about $8 while thrifting. I'm happy I didn't have 
plans to use battery power because the compartment was left with batteries in 
it for quite some time.

{% include image.html filename="coby-compartment.jpg"
    alt="Coby TV battery compartment" %}

Every battery was heavily damaged by rust.

{% include image.html filename="coby-batteries.jpg"
    alt="Coby TV battery rust" %}

The contacts were completely destroyed.

{% include image.html filename="coby-contacts.jpg"
    alt="Coby TV battery contacts" %}

Before dismantling the display I did a test to see if it was operational. To 
my delight the TV powered on, started relatively quickly, and showed sharp 
details in static.

{% include image.html filename="coby-test.jpg" alt="Coby TV power-on test" %}

The back of the Coby TV has a 3.5mm jack labeled "video in". I plugged a TS 
pigtail into this jack and a TRRS pigtail into the Pi's AV jack and proceeded 
to poke around. My initial guess of tip=video, sleeve=ground yielded a bouncy 
and bright picture on the tube. I played with the sync and brightness controls 
on the back until everything looked fairly clean.

{% include image.html filename="coby-terminal.jpg"
    alt="Linux terminal on Coby TV" %}

I packaged up the video cable by soldering the video wires and installing 
shrink tubing on each wire. I sliced a scrap cable sleeve and zip tied it to 
the sleeves of the joined cables for strain relief.

{% include image.html filename="video-cable.jpg" alt="Video Cable" %}

After verifying the screen was good to use I did a test fit in the cabinet. 
The entire TV fit without an issue, but the angle of the tube didn't match the 
angle of the panel removed from the cabinet.

{% include image.html filename="coby-vanilla-fit.jpg"
    alt="Original Coby TV fit" %}

I looked at the TV for a long bit contemplating whether I should tear it apart 
like I did with the RCA. I decided to keep the TV in tact so that it would be 
safer to work around and be more shielded to the environment. I did want to 
change the angle of the screen so I took a hacksaw to the battery compartment 
to adjust the TV's sitting angle.

{% include image.html filename="coby-hacked-up.jpg" alt="Modified Coby TV" %}

The change isn't extreme, but it's enough to make the screen feel much more 
natural. The hacksaw cuts weren't perfectly aligned on both sides, but this 
was easily fixed with some 40 grit sand paper.

{% include image.html filename="coby-before-after.jpg"
    alt="Coby TV before and after modification" %}

I also removed a small bit of wood from the bottom of the opening in the 
cabinet. This made it possible to push the TV against the opening in the 
cabinet without interference from any of the front-panel controls.

{% include image.html filename="coby-hacked-fit.jpg"
    alt="Modified Coby TV fit" %}

I think this new display will better help me achieve the retro-futuristic feel 
I want in this jukebox. I was excited for the color capability of the RCA TV, 
but I think the Coby's black and white, visibly rounded tube is a better fit. 
I am happy to be working on this project for the first time in a long while. 
Next log I'll be assembling all of the hardware modules and testing the 
assembled system.
