---
layout: document
title: The Lightning and Thunder Arrives
date: 2016-12-18 02:24:00 +0000
categories: [wasteland-radio, logs]
cover: audio-parts.jpg
---
Back in July I ordered power and audio components from Amazon. While taking 
pictures for another log I decided to post some pictures of goodies I 
received.

The original driver in the radio is a bit rusted up and has a busted cone, so 
I'm replacing it with modern car audio drivers. Two 4x6 Pioneer TSG4645R 
speakers will fill the left and right sides of the hole left by the original 
driver. A DROK TPA 3116 amplifier (far right) will receive input from a
Raspberry Pi and drive the new speakers. Also pictured is wire that came with 
the new drivers and a 3.5mm audio jumper.

{% include image.html filename="audio-parts.jpg" alt="Audio parts" %}

A DMiotech 24V DC power supply (top right) will accept mains power and provide 
power to the audio amplifier and Icstation DC to DC 5V USB buck converter 
(bottom right) that will power the Raspberry Pi. Also pictured is an AC power 
cable, an AC socket, A USB micro B cable, and some terminal strips  

{% include image.html filename="power-parts.jpg" alt="Power parts" %}
