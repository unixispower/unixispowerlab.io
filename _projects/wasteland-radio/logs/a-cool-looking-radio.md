---
layout: document
title: A Cool Looking Radio
date: 2016-07-02 08:20:00 +0000
categories: [wasteland-radio, logs]
cover: philco-42-390.jpg
---
Recently I spotted a cool looking Philco radio at a local junk store and 
snagged it for $40. I awkwardly loaded the spiderweb filled wooden box into my 
car and drove it home to my apartment. The label inside has the part number 
42-390; a nice radio for its time in 1942. The wooden cabinet shows a lot of 
water damage and aging so it's a perfect donor for a Fallout themed project.

{% include image.html filename="philco-42-390.jpg" alt="Philco 42-390" %}

The original circuitry was trashed, so I decided to start from the ground up 
and only recycle the cabinet. I removed the circuitry, faceplate, and speaker 
from the cabinet leaving only the wood and some mounting bolts behind.

{% include image.html filename="radio-gutted-back.jpg" alt="Back of gutted radio" %}

I think the radio may have served as a part donor at some point as all the 
tubes and many of the components have been removed.  

{% include image.html filename="radio-circuitry.jpg" alt="Radio circuitry" %}

I recruited my friend Kai to help and we managed to procure an air compressor 
from a kind stranger in town (small communities rock). After blowing 74 years 
of dust, spiderwebs, and dead bugs out with the compressor I wiped the 
remaining dirt out with a dry cloth. After all of this cleaning I glued the 
loose bits of wood and veneer down and threw a coat of furniture polish on.

{% include image.html filename="radio-gutted-front.jpg" alt="Front of gutted radio" %}

My scrap computer pile yielded a display that fits perfectly into the hole 
left by the removed radio unit. The donor unit is a Carry-I computer. I can 
find little information about this particular model, so I may have to perform 
some exploratory surgery to determine how to drive the display.

{% include image.html filename="carry-monitor.jpg" alt="Carry-I computer monitor" %}
