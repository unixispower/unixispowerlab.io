---
layout: document
title: New Decade New Electronics
date: 2020-07-13 18:50:00 +0000
categories: [trash-car, logs]
cover: stock-car.jpg
---
While visiting my mom a few weeks ago I decided to pull some of my old things
out of her basement to sell online. Among many goodies was an old remote
controlled car that I got when I was a kid. I don't remember how I obtained it,
but I do remember that it was missing the transmitter and was a planned future
project for myself. Now that I'm older and have a personal workspace I can
finally get to work making a fun toy to play with as an adult.

{% include image.html filename="stock-car.jpg"
    alt="Unmodified remote control car" %}

The first thing I did was take the car apart and strip the old electronics out.
I did so before deciding to post this project on my site, so I don't have any
pictures of the original receiver board or servo installed. The chassis is
fully plastic but has a suspension that works pretty well. The FCC ID on the
bottom is `L9G-47820R` which was
[granted to Artin Industrial Co Ltd in 1998](https://fccid.io/L9G47820R).

{% include image.html filename="stock-chassis.jpg"
    alt="Top and bottom of chassis with electronics removed" %}

I recently started to explore the world of RC planes with a friend and decided
to work with the [FlySky FS-i6X transmitter](https://www.flysky-cn.com/fsi6x)
and [Zippy Compact 1300 battery](https://hobbyking.com/en_us/zippy-compact-1300mah-3s-25c-lipo-pack.html)
I already had. I purchased a 6-channel
[FlySky FS-IA6B receiver](https://www.flysky-cn.com/ia6b-canshu), a
[Hobbywing QuicRun WP1060 Brushed ESC](https://www.hobbywingdirect.com/collections/quicrun-brushed-system),
and a
[pack of MG996R 55g servo clones](https://www.amazon.com/gp/product/B07MFK266B/)
to add to the car. I realize these parts are *completely overkill* but I wanted
to have them handy for the next build I work on. The only original electronic
component I reused was the brushed motor.

{% include image.html filename="stock-vs-new-electronics.jpg"
    alt="Stock and replacement electronics side-by-side" %}

The original steering servo is almost the same size of a standard servo. The
new servo was just slightly too long and slightly slimmer than the original.

{% include image.html filename="new-servo-fit.jpg"
    alt="New servo shown near where the old one was installed" %}

I had to remove pieces of the chassis as well as hack off the servo's mounting
tabs and reroute the cable through the bottom cover in order to make it fit.

{% include image.html filename="new-servo-mods.jpg"
    alt="New servo post-modification" %}

I remounted the original horn on the new servo to allow it to be a drop-in
replacement. The new servo's spline was a little bigger than the original so I
had to melt it a bit to make it fit.

{% include image.html filename="new-servo-old-horn.jpg"
    alt="New servo with old horn installed" %}

After a few modifications the new servo fit into the hole left by the old one.
The new servo was narrower and had to be padded with popsicle sticks to fit
snugly. I plan to come up with a better solution later -- I just had popsicles
on hand to work with.

{% include image.html filename="new-servo-installed.jpg"
    alt="New servo installed using popsicle stick shims" %}

Along with the servo replacement I soldered new wires to the brushed motor and
attached bullet connectors to make wiring the ESC easier. The ESC and receiver
were then mounted to the chassis and top shell using double-sided foam tape and
rubber bands were used to secure the shell to the chassis. I also lubed up all
the plastic bits with white lithium grease which loosened up the suspension
considerably.

{% include image.html filename="new-electronics-installed.jpg"
    alt="New electronics installed in the chassis" %}

I powered everything up and took it outside for a test. After reversing the
steering channel everything was good to go. I drove it around the street in
front of my apartment for a while and realized the steering was almost
unresponsive for most of the stick throw. After inspecting it a bit more I
figured out this was due to the way the servo is connected to the rods that
are attached to the wheels. Instead of directly controlling the rods, the servo
horn moves inside a shell that is attached to the rods. This indirect control
leaves a huge dead spot where the servo horn does not even touch the shell. In
the next log I plan to tackle this problem and clean up the mounting of the
servo a bit.
