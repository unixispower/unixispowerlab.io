---
layout: document
title: Butter Smooth Steering
date: 2020-08-22 10:29:00 +0000
categories: [trash-car, logs]
cover: takeout-pushrods.jpg
---
In the previous log I mentioned the large control dead-zone when steering the
car. I managed to fix this issue very easily using parts included with the new
servos and a bit of "trash" I had on hand. The old steering linkage rods, servo
horn, and plastic servo horn "cap" were removed and replaced with a new horn
directly attached to the wheels with a bit of wire taken from a Chinese food
takeout container. The new horn is one of the disc-type horns trimmed down
to be a quarter-disc so that it fit in the space left by the old one.

{% include image.html filename="takeout-pushrods.jpg"
    alt="Steering pushrods made out of takeout box wire" %}

This worked perfectly to eliminate the dead zone in the middle of the steering
stick on my transmitter but introduced a new issue by allowing *too* much
control. Driving the servo the full range possible caused the steering to bind
and pull on the front suspension.

{% include image.html filename="natural-steering.gif"
    alt="Animation of steering without servo limiting" %}

To fix this I limited the range of the steering axis on my transmitter to about
+80% and -80% of what it was. The new range prevents binding of the suspension
but still allows the wheels to turn to the same extremes as the servo's full
range.

{% include image.html filename="limited-steering.gif"
    alt="Animation of steering with servo limiting" %}

During the modification I got quite annoyed with the top shell being attached
to the chassis with the short wires running from the receiver to the other
hardware. To remedy this I migrated all hardware to the chassis and used a bit
more of the takeout wire to create a retainer for the wiring. I attached the
retainer to one of the old PCB mounting posts with a screw pulled from the old
PCB.

{% include image.html filename="repositioned-hardware.jpg"
    alt="Hardware inside the car chassis with a retaining wire installed" %}

The new steering system is tons more sensitive than the old was. I am now able
to finely control the car at low speeds as well as drive in tighter circles.
This thing is starting to feel like a nice little toy. Now I just need to add
some accessories to make driving more fun.
