---
layout: project
title: Pocket Guestbook
date: 2020-08-22 08:01:00 +0000
log_date: 2020-08-22 08:01:00 +0000
icon: icon.gif
flair: complete
cover: guestbook-hardware.jpg
links:
  - title: Device Firmware
    description: Arduino-based firmware for the ESP8266
    url: https://gitlab.com/unixispower/pocket-guestbook
---
An ESP8266-based web server that hosts an offline guestbook from an SD card.
This was an experiment to create interactive web application on an
embedded system with minimal "server-side" data processing.

{% include image.html filename="guestbook-hardware.jpg"
    alt="Pocket guestbook hardware" %}

The device hosts an access point and acts as a captive portal to redirect all
connected clients to the guestbook page. The hardware is a WeMos D1 Mini clone
with a micro to full-size SD card adapter soldered directly to it. AP settings,
static web files, and persisted guest entries are all stored on the SD card.
