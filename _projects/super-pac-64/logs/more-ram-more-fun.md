---
layout: document
title: More RAM More Fun
date: 2021-06-26 02:09:29 +0000
categories: [super-pac-64, logs]
cover: 64mb-ram-upgrade.jpg
---
When I started playing with the PAC I had a set of goals in mind for upgrades.
I wanted to add a wireless networking adapter, better internal speakers,
upgrade the disk to flash, and max out the RAM. Last week I ordered the last
item on my upgrade list: two sticks of 32MB EDO RAM. This addition should allow
me to use some nice soundfonts that I didn't consider before for fear of using
too much memory.

{% include image.html filename="64mb-ram-upgrade.jpg"
    alt="Two sticks of 32MB 72-pin EDO memory" %}

I installed the sticks in the 2 unpopulated slots on the motherboard. The RAM
was fairly easy to install, but required removing the power supply and 
rerouting the cable mess that previously filled the empty RAM slots. The new
sticks were a bit taller than the old ones, but still short enough to allow the
power supply to be bolted back into the case. The PAC now contains a total of
128MB of RAM which is the maximum supported by the `AI5TV` motherboard. I had
no issues with the new memory and it was available to use under Windows as soon
as I booted up.

{% include image.html filename="128mb-ram-installed.png"
    alt="Windows showing a total of 128MB memory installed" %}

I celebrated the completion of my upgrade list by installing some old games and
playing around a bit with some software. I had some game CDs on hand from
shopping at local thrift stores so I loaded some up for testing. I tried
SimCity 2000 Special Edition, SimAnt, Dragon Dice, Doom Collectors Edition
(original trilogy), Hexen, and Quake 3 Arena.

{% include image.html filename="assorted-games.jpg"
    alt="Assorted games for DOS and early Windows" %}

Installing and test playing everything went pretty smoothly with a couple of
exceptions. Quake 3 Arena complained that the CPU was under the minimum of
233MHz but still allowed install. When I tried to run the game I received an
OpenGL error. This isn't surprising as the Chips and Technologies 65548 isn't
exactly a graphics powerhouse and doesn't have hardware accelerated OpenGL
drivers.

{% include image.html filename="quake-3-arena-error.png"
    alt="Quake 3 Arena launcher showing OpenGL error" %}

The other issue I encountered with the game CDs was with Dragon Dice. This is
a disc that I have previously gotten to work in other computers, but the Teac
drive in the PAC didn't like it at all. After inserting the CD the drive spun
up with a loud hum and lots of vibration. Windows stopped responding at this
point and only showed cursor movement with no other display updates. After a
couple of minutes the LCD flashed to a text-mode error page instructing me to
check the disc. I didn't waste much time on this issue because it seemed to be
related to the disc.

{% include image.html filename="dragon-dice-error.jpg"
    alt="CD error screen shown when Dragon Dice CD is inserted" %}

Unsurprisingly the PAC chewed through Doom without breaking a sweat. It only
took a couple levels for me to realize I wanted something a little better than
the soundfont that is included with the Creative drivers. I did a bit of
searching and found a few soundfonts to try out with the Sound Blaster Live.
I found one called [GeneralUser GS](http://www.schristiancollins.com/generaluser.php)
that has a version designed specifically to work with the SBL series of cards
and sounds very clean. I also found one called
[8MBGM ENHANCED v1.8](http://www.bredel.homepage.t-online.de/Soundfonts/Soundfonts-English/soundfonts-english.html)
that works with the SBL and sounds a bit "rougher" than GeneralUser GS (in a
very good way).

I found a few newer soundfonts, but it seems that some of them are incompatible
with the SBL for whatever reason. Some of the soundfonts had broken instruments
that only played certain notes and others had voices that were played
incorrectly. Many of the newer soundfonts are larger than 32MB making them a
bit heavy for use on a system with a total of 128MB of RAM.

After seeing good performance in Doom I wanted to see how the PAC handled a
more "intense" 3D game so I grabbed a copy of the original Quake and ran
`timedemo demo1` in the game console. At 360x240 the PAC ran the demo in 40.4
seconds averaging 24 fps. Actually playing the game feels pretty responsive
with some noticeable frame rate dips when there is a lot going on.

I also made Windows a bit more comfy by setting up one of the desktop themes
that was included with Windows 98. After much deliberation I went with my old
favorite "Inside your Computer".

{% include image.html filename="inside-your-computer-theme.png"
    alt="Inside your Computer Windows 98 theme" %}

At this point the PAC feels pretty complete upgrade-wise. There are still
things I would like to work on, but the remaining issues are with aging
hardware that I have been able to work around or ignore. A couple such
annoyances are a yellowed LCD and a dodgy brightness slider, but neither of
those affect performance or cause issues. I didn't create a Windows 9X
performance monster, but I did manage to turn a broken network sniffer into a
portable workstation that will be useful when interfacing with legacy equipment
(and for some retro gaming goodness). 
