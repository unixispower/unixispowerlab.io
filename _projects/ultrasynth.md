---
layout: project
title: Ultrasound Synthesizer
date: 2022-05-06 17:55:00 +0000
log_date: 2022-07-18 01:43:43 +0000
icon: icon.gif
flair: complete
cover: scan.jpg
links:
  - title: Ultrasound Synthesizer Script
    description: Python script that generates audio from a spectral Doppler
      image
    url: https://gitlab.com/unixispower/ultrasynth
---
A Python script that synthesizes audio from an spectral Doppler image generated
by an ultrasound machine.

Some ultrasound machines are capable of measuring the flow of blood and can
generate a graphical and audible representation of that flow. These machines
are often used to measure fetal heartbeats and give expecting mothers a way to
see and hear the heartbeat of an unborn child. Doctors can provide a printout
from these machines to take home, but audio recordings aren't usually provided.

A friend posted on Facebook with a picture of the printout she received from one
such ultrasound machine. She didn't take a recording with her phone at the time
so all she had was a piece of paper that showed a spectrogram image. I did some
research on ultrasound machines and hacked together a Python script that can
process a scan of a printout and recreate the audio portion.

Here is the image I received to work with.

{% include image.html filename="scan.jpg"
    alt="" %}

The script processes just the portion of the image above the axis line so I
rotated, cropped, and cleaned it up using GIMP.

{% include image.html filename="cleaned-scan.png"
    alt="" %}

The cleaned image was then processed and the following audio was generated.

{% include audio.html filename="output.wav" %}

Full instructions on installation and usage of the script are included in the
link below.

Note: the first 2 logs were written long before they were published. The dates
on them vary slightly so they sort correctly.
