---
layout: project
title: Tape Artchive
date: 2017-08-17 21:24:00 +0000
log_date: 2017-10-28 01:56:00 +0000
icon: icon.gif
flair: complete
links:
  - title: Tape Generator
    description: Tape generator makefile with example content
    url: https://gitlab.com/unixispower/tape-artchive
---
An experiment to store visual art with audio on cassette tape by using amateur
radio SSTV signals. Quite impractical, but it made for a nice gift to a friend
who shares an interest in tape.

{% include video.html service="youtube" id="aFI95TEXlQ4" aspect="16:9" %}
