---
layout: document
title: Square Peg, Unknown Hole
date: 2017-11-02 00:56:00 +0000
categories: [datape, logs]
cover: 2400bps.jpg
---
Today I ran some tests recording different rates of pulses on tape. I wrote a 
small python utility that accepts a string of bits (11011000100110...) and a 
bitrate and generates WAV file containing the bits as return-to-zero coded 
square wave pulses. I generated 4 files of different bit rates and recorded 
them all to tape using my Technics RS-T230. I played the tape back into the 
computer and recorded the input. All playback and recording was done 
using Audacity with a sample rate of 48000 samples/sec.

I tested the bit rates 300, 1200, 2400, and 4800. What I noticed is that the 
tape recorder does not like square waves much _at all_. The tests look 
somewhat promising, but the waveform does not hold value at the maximum value 
of the square wave.

300 bits/sec:


{% include image.html filename="300bps.jpg" alt="300 bits/sec test" %}

1200 bits/sec:

{% include image.html filename="1200bps.jpg" alt="1200 bits/sec test" %}

2400 bits/sec:

{% include image.html filename="2400bps.jpg" alt="2400 bits/sec test" %}

4800 bits/sec:

{% include image.html filename="4800bps.jpg" alt="4800 bits/sec test" %}

In higher bitrate tests distortion is less of an issue, but weirdness still
exists. My next experiments will be with pulse-shaping. I think finding a 
waveform closer to a sine wave will help reduce the amount of distortion from 
the the tape deck.
