---
layout: document
title: On the Cusp
date: 2017-11-05 05:42:00 +0000
categories: [datape, logs]
cover: cusp-sketch.jpg
---
After completing some more experiments with pulse shaping I've learned a 
little more about my particular tape deck. I tweaked my bit generator script a 
bit more and tested out some waveforms with intentionally-introduced cusps. 
The idea was to encode bits as a modified sine wave (or similar) and introduce 
a cusp in the direction of the encoded bit.

{% include image.html filename="cusp-sketch.jpg"
    alt="Sketch of cusp waveform" %}

I chose to use x^n as the function for generating the cusp-encoded data. The 
generator script was modified to encode a string of bits as individual pulses 
consisting of x^n mirrored over the length of the pulse. I chose several 
different values of n (2,3,4) and several different data rates (300, 1200, 
2400, 4800) to test. What I found is that cusps are preserved well at lower 
data rates, but are rounded off at higher ones.

x^2 at 300 bits/sec:

{% include image.html filename="x2-300bps.jpg"
    alt="x^2 waveform at 300 bits/sec" %}

x^4 at 300 bits/sec:

{% include image.html filename="x4-300bps.jpg"
    alt="x^4 waveform at 300 bits/sec" %}

x^2 at 4800 bits/sec:

{% include image.html filename="x2-4800bps.jpg"
    alt="x^2 waveform at 4800 bits/sec" %}

x^4 at 4800 bits/sec:

{% include image.html filename="x4-4800bps.jpg"
    alt="x^4 waveform at 4800 bits/sec" %}

I am a bit disappointed that these tests didn't work out as well as I hoped. I 
did notice that at higher frequencies (higher data rates) the issue of the 
signal "self-aligning" around 0v is minimized. Even though my goal is to find 
an encoding that is entirely self-clocked (and therefore timing-independent), 
I may end up targeting a minimum data rate for future tests. Perhaps the 
increased frequency of the encoded data will stabilize the DC offset issues.

I may also try some form of modulation to create waveforms the deck likes, but 
most modulation schemes I've looked at expect somewhat precise frequencies or 
clock synchronization between systems. The varying speed of tape playback may 
mean I have to fight frequency and clock errors with a reference tone in the 
second stereo channel. I'd like to avoid anything that requires both channels 
to be sampled since all of my computers (and many other modern consumer 
models) only have mono audio input.

One other possibility I've been thinking about is to encode data onto both 
stereo channels and mixing them with a stereo to mono additive mixer before 
sampling with the computer. Mixing in hardware has the drawback of requiring 
custom cable, but such a cable would be very easy to make with an AUX cable 
and a couple of resistors.
