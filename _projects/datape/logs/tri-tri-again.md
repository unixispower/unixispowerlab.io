---
layout: document
title: Tri, Tri Again
date: 2017-11-10 08:45:00 +0000
categories: [datape, logs]
cover: cusp-artifact.jpg
---
In my previous test I tried encoding information using cusps to no avail. The 
tape player rounded off sharp changes at higher frequencies, but did a decent 
job at preserving the general shape of the waveform. The only major 
distortions that appeared were at zero-crossings near bit changes and during 
long runs of 1s or 0s.

{% include image.html filename="cusp-artifact.jpg"
    alt="Recording artifact caused by waveform cusps" %}

I simplified the x^2 waveform to a triangle wave to avoid slope changes near 
zero when bit flips occur. I tested this waveform with the speeds I have tried 
in other tests: 300, 1200, 2400, 4800 bits/sec. I used a test pattern of 
`010100110000111100000000...` to see how the DC offset issue affects long runs 
of repeating bits.

300 bits/sec:

{% include image.html filename="300bps.jpg"
    alt="Triangle waveform at 300 bits/sec" %}

1200 bits/sec:  

{% include image.html filename="1200bps.jpg"
    alt="Triangle waveform at 1200 bits/sec" %}

2400 bits/sec:  

{% include image.html filename="2400bps.jpg"
    alt="Triangle waveform at 2400 bits/sec" %}

4800 bits/sec:  

{% include image.html filename="4800bps.jpg"
    alt="Triangle waveform at 4800 bits/sec" %}

At higher bit rates the DC centering seems to occur less, but is still an 
issue for long runs of the same bit. I decided to tests at 9600 bits/sec to 
see if even higher speeds would help more:

9600 bits/sec:

{% include image.html filename="9600bps.jpg"
    alt="Triangle waveform at 9600 bits/sec" %}

It appears that a simple triangle pulse shape might be a feasible start to 
high data rate tape storage. Because of the DC centering issues I'll be 
limited to fairly high data rates (for tape) of 2400 bits/sec and above. My 
script has some issues with creating smooth transitions at 9600 bits/sec, but 
I'm not really worried about that because soon I'll be starting encoder and 
decoder development in C. My next task will be finding a balanced
[line code](https://en.wikipedia.org/wiki/Line_code) to try and combat DC 
offset issues.
