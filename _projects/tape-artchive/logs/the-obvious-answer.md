---
layout: document
title: The Obvious Answer
date: 2017-10-01 06:00:00 +0000
categories: [tape-artchive, logs]
cover: recording-setup.jpg
---
I often assume the worst when I shouldn't. My last log stated that I would 
likely have to build a line to mic adapter to use the Panasonic tape recorder 
with an external input. I am happy to say I was wrong; the Panasonic tape deck 
has an auxiliary input.

{% include image.html filename="panasonic-front.jpg"
    alt="Panasonic tape deck front view" %}

At first I didn't realize the recorder had an external input. I began the hard 
way by taking the device apart and trying to understand the circuitry. After a 
long while of tracing leads with a flashlight and marker I decided it would be 
faster to understand the functionality of the device by experimenting with it.

{% include image.html filename="panasonic-inside.jpg"
    alt="Panasonic tape deck circuit board" %}

I first connected a small speaker to the back of the deck, popped a fresh 
cassette in, then switched on the radio and pressed record. To my 
disappointment the ribbon and wheels in the tape sat motionless. The unit was 
still open from my attempted circuit exploration so I decided to play around 
with the tape mechanism to see if it would budge. On a whim I nudged the 
flywheel on the back of the tape compartment (see green arrow below) which 
caused the assembly to "click" and start moving as it should.  

{% include image.html filename="panasonic-flywheel.jpg"
    alt="Panasonic tape deck flywheel" %}

I started audacity on my laptop and played one of the recordings generated by 
the makefile mentioned in the previous log. To my delight the deck not only 
played the auxiliary input through the small speaker, but also recorded it to 
tape.

{% include image.html filename="recording-setup.jpg"
    alt="Temporary recording setup" %}

The joy of having a working recorder was negated by the almost non-responsive 
VU meter on the front of the deck. I tested the playback of the tape on my 
Walkman to find the levels to be extremely low and one channel to be louder 
than the other. The laptop I used for recording does a poor job at driving 
headphones very loudly which may explain the low volume issue.

I also noticed that pressing the "AUX" button on the front of the deck caused 
the VU meter to fluctuate between one bar and complete silence. I think this 
is caused by dirty contacts in the input selection switch and my also be the 
cause of the channel balance issue.

Admittedly this isn't an ideal setup for recording any reliable tests. Next 
time I plan to clean the contacts of the "AUX" switch and try recording again 
with a USB sound card to see if I can get better results.
