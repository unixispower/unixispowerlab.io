---
layout: document
title: Test Unrest
date: 2017-10-07 22:55:00 +0000
categories: [tape-artchive, logs]
cover: elastic-failures.jpg
---
At the end of the previous log I stated I reduced the flutter of the Sony tape 
deck to almost nothing. This was a hopeful statement that was proven wrong 
when I tested out the deck with a different recording. The recording I 
initially tested with was full of low range instruments with vibrato, which 
made it difficult to detect the flutter that was present. When I recorded 
piano music a high-pitch flutter was noticeable on long notes.

I eliminated a lot of the flutter by switching to a less elastic belt 
material, but a good amount of it remained. I thought the remaining flutter 
was due to the natural shape the belts take because of the way the bracelet 
cord is packaged. The cord is wrapped around a cardboard back which causes 
visible bends every few inches. The belts made with the cord take an oval 
shape instead of a perfectly round one.

{% include image.html filename="deformed-belts.jpg"
    alt="Deformed elastic belts" %}

These bends could cause the speed of the motor to slightly change when they 
are flattened around a pulley causing a small frequency deviation. To try and 
fix this I straightened a length of cord and made another set of belts.

{% include image.html filename="deformed-elastic.jpg"
    alt="Deformed elastic cord" %}

To straighten the cord I placed it in a bath of boiling water for about 30 
seconds then removed and stretched it. I repeated this process 5 times until I 
had a fairly straight cord.

{% include image.html filename="boiled-elastic.jpg" alt="Boiled elastic cord" %}

I cut and measured the belts from the previous log since they appeared to be 
around right size. The capstan belt measured 143mm, and the fast-forward belt 
measured 176mm. I made new belts from the straightened cord. You can see how 
the new belts (pink) look much more round than the old belts (blue).

{% include image.html filename="deformed-vs-boiled.jpg"
    alt="Deformed and boiled elastic cord" %}

I ran another testing using
[Bach's Fugue in G Minor](https://www.youtube.com/watch?v=ddbxFi3-UO4). To my
disappointment these belts caused the same flutter the other ones did. I tried 
making different measurements of belts to see what effect they had on the 
flutter. First I tried a larger capstan belt, but the flutter worsened and 
reduced in frequency. This led me to believe a shorted belt would help.

I tried several more sizes of belt without much of an improvement. The results 
ranged from very slow flutter with looser belts, to a fast flutter with tight 
belts. The flutter never completely left, and got worse as I tried smaller and 
smaller belts.

{% include image.html filename="elastic-failures.jpg"
    alt="Pile of unsuccessful elastic cord belts" %}

It seems that the elastic cord wasn't as good as I thought it was in the
previous log. I'm giving up on making belts for now to focus on other parts of 
the project. Hopefully I'll be able to buy a large pack of _real_ belts 
soon and try with those.
