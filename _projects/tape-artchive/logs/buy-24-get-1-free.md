---
layout: document
title: Buy 24 Get 1 Free
date: 2017-08-26 20:19:00 +0000
categories: [tape-artchive, logs]
cover: blank-tapes.jpg
---
Yesterday I found a great score at the local vintage video game store. I 
managed to snag 24 new blank cassette tapes $13 and a cassette recorder for 
free. Almost all of the blank tapes are Memorex DBS 90 with the exception being
2 TDK D90. Both types of tape are "Type I" meaning they utilize iron oxide as 
the ferromagnetic material.

{% include image.html filename="blank-tapes.jpg" alt="Blank tapes" %}

I cleared out all of the tapes the shop had since they were priced at $0.50 
each. The employee that sold me the tapes threw in a free recorder that was 
destined for the landfill. 

{% include image.html filename="panasonic-top.jpg" alt="Panasonic tape player top view" %}

The recorder is a Panasonic model SG-P100. Since the record function is 
intended for microphone use I'm going to assume this is a mono recorder. This 
unit also contains an AM and FM stereo as well as a phono amplifier.

{% include image.html filename="panasonic-bottom.jpg" alt="Panasonic tape player bottom view" %}

I'll have to make a line-to-mic level adapter for cassette recorder since it 
has a microphone port instead of a line-in. The adapter for the recorder 
should be simpler to make than the one for mobile phones. I just need TRS and 
TRRS patch cables to sacrifice for adapters and I can begin recording.
