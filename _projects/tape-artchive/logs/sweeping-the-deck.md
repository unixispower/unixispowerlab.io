---
layout: document
title: Sweeping the Deck
date: 2017-10-03 23:47:00 +0000
categories: [tape-artchive, logs]
cover: panasonic-vu-meter.jpg
---
Recording information to a tape would be difficult or impossible with a dodgy 
setup. There are two issues with the recorder: the low audio level and the 
dirty AUX switch that yields a random audio level every time it's 
toggled.

To fix the low level issue I decided to try out driving the recorder with a 
USB sound card instead of with the laptop's builtin one. The laptop at max 
volume would only occasionally illuminate the -15dB LED on the "peak level" 
meter, while the USB sound card illuminates the +3dB LED at a little over half 
volume. These are rough measurements because I made them while gently 
"massaging" the AUX button so I could get a glimpse of the max peak levels.

{% include image.html filename="usb-sound-card.jpg"
    alt="Sabrent USB sound card" %}

The scratchy button was surprisingly easy to fix. The buttons didn't look easy 
to access or disassemble, so I tried a less direct method of cleaning. I 
pressed and released the AUX button rapidly for about a minute. After cleaning 
the contacts this way the random-level audio issue disappeared. My guess is 
that a thin layer of oxidation built up on the switch contacts over years of 
non-use. Depressing the switch quickly may have scratched some of that 
oxidation off, resulting in a better connection.

{% include image.html filename="panasonic-vu-meter.jpg"
    alt="Panasonic VU meter" %}

Since I hope to use this recorder for other projects I tested its response to 
the full recording range from 20Hz to 20kHz. I'm curious about the frequency 
response of the device and I'd like to see if there is any obvious distortion 
to the recorded waveform.

The test procedure was as follows: first record the sweep on to a cassette, 
then record the playback of the sweep with the computer, and finally compare 
the two recordings to approximate a response curve for the recorder. I used a 
sweep file I found on the
[UBC Physics website](http://www.theory.physics.ubc.ca/341-current/sweep.html)
for the test. The level of the sound card was set so the VU meter of the tape
recorder read 0dB during the tape recording. The level of the tape player was 
set so audacity read 0db during the computer recording. The tape deck's Dolby 
NR setting was set to "out" for recording and playback so the waveform being 
recorded on the tape would not be modified. Below is a screenshot of the 
played and recorded sweep spectrograms in Audacity:

{% include image.html filename="panasonic-sweep.jpg"
    alt="Panasonic deck sweep" %}

Bandwidth of SSTV signals is pretty narrow and frequencies used fall within 
1200Hz-2300Hz (see 
[this resource](http://www.sstv-handbook.com/download/sstv_04.pdf) on 
[SSTV stuff](http://www.sstv-handbook.com/)). This range on the recorder looks 
pretty uniform, but shows a strange (harmonic?) frequency. I am very curious 
to see what kind of image artifacts are caused by the characteristics of this 
recording setup.

This is a flawed test as I can't ensure my cheapo USB sound card has anywhere 
near a flat response, so I don't know where the recording artifacts are coming 
from. This test does, however, give me an idea of how this particular setup 
will perform for recording.  Next time I'll be digging into testing 
individual SSTV formats with this setup.
