---
layout: document
title: Swapping the Deck
date: 2017-10-06 02:09:00 +0000
categories: [tape-artchive, logs]
cover: sony-no-belts.jpg
---
The end of the previous log stated I would be working on format comparisons 
this log, but that isn't true. This log will be focused on the Sony 
TC-FX160 stereo cassette deck that I picked up at a thrift shop 
today.

{% include image.html filename="sony-front.jpg"
    alt="Sony tape deck front view" %}

This deck appears newer than the Panasonic deck from my previous logs. The 
Sony deck is a single-purpose device where the Panasonic was a radio, phono 
preamp, and amplifier. I plan to use the better deck for recording and put
the runner-up aside for a future project.

As I expected from a thrift shop find, the Sony deck didn't work. I tried 
playing a tape, but nothing in the player moved. I did hear a motor start when 
I pressed play, so I took the unit apart to look at the belts.

{% include image.html filename="sony-no-belts.jpg"
    alt="Tape mechanism without belts installed" %}

At first it looked like the belts were broken, but when I tried to remove them 
I got a surprise; they had gone so soft the texture was like uncured silicone. 
The belts disintegrated as I tried to remove it from the pulleys. I had to 
scrape for a little while to get most of the goop cleaned out.

{% include image.html filename="belt-goo.gif"
    alt="Animation of belt disintegrating" %}

I googled around a bit and found that rubber bands or string can be used as 
replacement belts. I didn't know how to install the bands as belts on this 
particular model because the old belts were completely gone. I couldn't find a 
service manual for this model, so I had to use the remnants of the old belts 
as a guide to place the rubber bands.

{% include image.html filename="sony-band-belts.jpg"
    alt="Tape mechanism with rubber band belts" %}

This seemed to work. The Sony deck will now play and rewind, but will not fast 
forward. I think this is because the rubber band attaching the top pulley to 
the motor pulls the pulley so that it does not fully engage when the 
fast-forward button is pressed.

I decided to run the same test as I did on the Panasonic to approximate the 
frequency response of the Sony deck. The following is a screenshot of audacity 
showing the spectrograms of a 20kHz to 20Hz sweeps on the decks.

{% include image.html filename="sweep-comparison.jpg"
    alt="Comparison of Panasonic and Sony deck sweeps" %}

The Sony responds to higher frequencies than the Panasonic. The Sony also 
sounds better and has much less noise. I'll be using the Sony deck to record 
the tape tests and the final tape (unless I see something _really_ good out in 
the wild).
