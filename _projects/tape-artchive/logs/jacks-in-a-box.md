---
layout: document
title: Jacks in a Box
date: 2017-10-08 05:48:00 +0000
categories: [tape-artchive, logs]
cover: adapter-pcb.jpg
---
I managed to pick up a few critical parts while out shopping today. I found a 
3.5mm TRRS auxiliary cable and a small mint tin to act as an enclosure for the 
cassette-to-phone adapter.

{% include image.html filename="cable-and-box.jpg"
    alt="Mint tin box and audio cable" %}

The original purpose of the adapter was just to adjust the signal out of the 
tape player to the level of a smartphone microphone input. I dislike the 
idea of only using a single stereo channel, so I decided to use the extra 
channel for music or commentary. I came up with a design that allows the left 
tape channel to be used for SSTV signals and the right tape channel to drive a 
pair of headphones.  

{% include image.html filename="adapter-schematic.png"
    alt="Adapter schematic" %}

This adapter tricks a smartphone into detecting a connected microphone by 
placing a resistance across the second ring and sleeve of the TRRS connector. A
capacitor prevents DC current from traveling between the player and phone and 
prevents the attenuator created by the 10kΩ and 100Ω resistor from 
decreasing the resistance observed by the phone. This capacitor also creates a 
high-pass filter combined with the 2.2kΩ resistor, but the filter has a cutoff 
frequency around 7Hz so performance should be unaffected.

I used a small breadboard to prototype the circuit. I cut the TRRS cable in 
half and soldered each side to header pins to make them easier to work with. I 
bridged the second ring and sleeve of the TRRS connector going to the tape 
player because I needed it to behave like a TRS connector and didn't know 
which contact the tape player's headphone jack would connect to.  

{% include image.html filename="adapter-breadboard.jpg"
    alt="Adapter breadboard prototype" %}

I tested this adapter and found it to work with my Honor 5x Android phone as 
well as an iPhone 5S. After testing, I put assembled the circuit on a small 
piece of protoboard and placed it in the metal mint tin. The wires are
secured with zip-ties that are epoxied to the inside of the tin. This provides 
no strain relief, but does secure the wires from being moved.  

{% include image.html filename="adapter-pcb.jpg" alt="PCB in mint tin" %}

I also added added a small screw to keep the lid from accidentally being 
opened. Here is the adapter ready for labels.

{% include image.html filename="adapter-finished.jpg" alt="Finished adapter" %}

I'm still waiting on belts for the Sony tape deck to arrive. Once I have those 
I'll be able to run some format tests. While I wait I'm going to work on 
adding the commentary/music track support to the
[make script](https://gitlab.com/unixispower/tape-artchive).
