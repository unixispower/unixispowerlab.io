---
layout: document
title: Belts Off
date: 2017-10-06 23:28:00 +0000
categories: [tape-artchive, logs]
cover: bands-installed.jpg
---
Last night I got the Sony deck working again by replacing the missing belts 
with rubber bands. This _seemed_ to work, but introduced a lot of flutter, and 
put tension on the pulleys. This made me unhappy so I went in search of a 
better solution. I scoured [Hifi Engine](https://www.hifiengine.com) for a 
service manual, but I didn't find an exact match. I did find a few units 
similar in appearance; notably the
[TC-FX110](https://www.hifiengine.com/manual_library/sony/tc-fx110.shtml). The 
exploded view of the cassette mechanism looked like the one in the TC-FX160:

{% include image.html filename="mechanism-exploded.png"
    alt="Cassette mechanism exploded view" %}

I goofed by guessing the belt placement in the previous log. The exploded view 
shows the correct placement of the belts and also shows part numbers for 
the belts (3-390-855-01, 3-390-816-01). I googled around a bit for 
replacements, but decided to try out a different rubber band approach.

{% include image.html filename="band-split.jpg"
    alt="Rubber band split using a razor blade" %}

I split a large folder band into two almost square strips that looked about 
the same size as the original. I wrapped the band around each set of pulleys 
and cut the two belts from it. I joined these belts at the edges with 
superglue and marked each with a different color to mark where they went.

{% include image.html filename="band-belts.jpg"
    alt="Rubber band belts ready to install" %}

I installed the bands according to the exploded view. The belts were much 
easier to install this time around.

{% include image.html filename="bands-installed.jpg"
    alt="Rubber band belts installed" %}

The new "belts" made the problem _worse_. Flutter was now very obvious on 
the tape I tested with. Not all was bad, though; fast-forward was no longer 
broken. I think this arrangement of the belts is correct, but the elasticity 
of the rubber bands causes the flywheel to fluctuate in speed.

I tried replacing the rubber bands with thread tied onto the pulleys. This 
looked promising with no tape in, but the tread slipped under load.

{% include image.html filename="thread-installed.jpg"
    alt="Thread belts installed" %}

Not wanting to give up I went to Wal-Mart to look for potential belt 
materials. I found a product called "Princess Elastic Cord".

{% include image.html filename="elastic-cord.jpg"
    alt="Package of elastic cord" %}

This is a cord made of a rubber-like material, but seems less elastic than 
rubber bands. I used the rubber band belts as a guide to cut belts from the 
elastic cord.

{% include image.html filename="bands-vs-elastic.jpg"
    alt="Rubber band belts next to elastic belts" %}

I installed the belts and played the test tape. The flutter was very reduced 
-- but still there. The belt driving the capstan was a little loose, so I 
removed a couple centimeters and tried again.

{% include image.html filename="elastic-installed.jpg"
    alt="Elastic belts installed" %}

<del>This was it! The test tape had very little audible flutter compared to 
other playthroughs (I don't have golden ears, YMMV). I'm curious to see how
these belts hold up. Now that I have a working recorder I can focus on getting 
the sound from a tape player into a mobile phone.</del>

This was not it. The recording I used for testing fooled me into thinking 
there was very little flutter. I have since tried a different recording and 
found the elastic belts to be very poor. The next log has more details about 
this realization.
