---
layout: document
title: Makeing Some Noise
date: 2017-08-19 03:47:00 +0000
categories: [tape-artchive, logs]
cover: airgap-test.png
---
I don't have the necessary hardware to move forward with a cassette-phone 
adapter, so I've decided to work on the soft side of things. To make recording 
of the cassette easier I created a script that takes a number of images and 
produces a single audio file containing the SSTV audio of all of the encoded 
images. 

This task is a compilation of sorts: source images are "compiled" into SSTV 
WAV files that are then "linked" into a single large WAV file. Rather than 
create a shell script I decided to use GNU Make to create the sound file. Make 
seemed to be a good fit for converting a number of files into several 
intermediate formats and combining them.

I found a Python based program called [PySSTV](https://github.com/dnet/pySSTV) 
that encodes images into a variety of SSTV formats, but not without a catch. 
PySSTV only uses the portion of the input image that is visible at 
pixel-to-pixel scale with the SSTV canvas. This means the input images need to 
be resized with [ImageMagick](https://www.imagemagick.org/script/index.php) 
before being converted. Once the images are resized and converted to WAV files 
they are concatenated using [SoX](http://sox.sourceforge.net/). SoX is also 
used to create a small bit of silence that is placed after every WAV file in 
the concatenation. This silence allows the current slide to be viewed before 
starting a new slide.

After writing up this makefile I tested the output sound file by playing some 
audio through my laptop's speakers to air-gap the slideshow to Robot36 on my 
phone. The quality is pretty poor because of the air-gap, but the slideshow 
progressed as expected. Below is one of the images captured from the playback.

{% include image.html filename="airgap-test.png" alt="Airgap SSTV test" %}

The [makefile and sample slides](https://gitlab.com/unixispower/tape-artchive)
are published on GitHub for anyone who is interested to see the solution or 
for anyone who wants to generate their own artchive. I have also included 
instructions on how to record a tape with the output WAV file.
