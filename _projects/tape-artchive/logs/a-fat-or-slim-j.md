---
layout: document
title: A Fat (or Slim) J
date: 2017-08-31 05:38:00 +0000
categories: [tape-artchive, logs]
cover: jcard-front.jpg
---
More procrastination ensues. I still haven't picked up audio cables for 
recording the cassettes, but I have made a little more software. I just 
finished creating a printable in-browser j-card template. Here is the template 
printed on orange card-stock:

{% include image.html filename="jcard-front.jpg" alt="J-card front view" %}

I initially created the template for standard cassette boxes that have a 1" 
back panel. After printing a test page I realized my Memorex cassette cases 
have a back panel that isn't quite 1" tall. I added an option to the template 
to shorten the back panel to 0.6" to support these cases. Here is the short 
card back in a Memorex case:

{% include image.html filename="jcard-back.jpg" alt="J-card back view" %}

I'm happy with this test print, and will use this template to create the 
packaging for the final tape. Feel free to use this template in your own 
projects however you wish.
