---
layout: collection
title: Personal Projects
item_collection: projects
item_sort_by: log_date
item_template: icon_preview.html
---
I like to work on electronics and write software to meet immediate "needs" as
well as to have some fun (okay, mostly for fun). Here are some of my projects.
