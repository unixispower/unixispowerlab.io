---
layout: collection
title: Weblog Posts
item_collection: posts
item_template: inline_document.html
item_count: 10
next_path: posts/archive.md
---
Updates about the site and general thoughts. The following are my most recent
posts.
