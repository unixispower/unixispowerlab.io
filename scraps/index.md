---
layout: collection
title: Scrap Pieces
item_collection: scraps
item_template: icon_preview.html
---
These things aren't quite full projects, but lesser efforts that are still
worth posting. Here are the scripts, hacks, modifications, explorations, and
other miscellany.
